extends Control

var tutorial = preload("res://Levels/Tutorial.tscn")
var first_level = preload("res://Levels/Level2.tscn")
 
func _on_TutorialButton_pressed():
	var level = tutorial.instance()
	get_tree().get_root().add_child(level)
	self.queue_free()

func _on_StartGameButton_pressed():
	get_tree().paused = false
	var level = first_level.instance()
	get_tree().get_root().add_child(level)
	self.queue_free()
