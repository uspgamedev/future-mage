extends State

var damage_box
var time: float = 0.0
var displacement

func enter(obj):
	subject = obj
	damage_box = subject.get_node("DamageBox")	
	damage_box.connect("body_entered", self, "_damagebox_entered")

func update(delta):
	#print(damage_box.position)
	displacement = 300 * delta
	time += delta
	
	if time >= 0.5:
		displacement *= -1
	
	if subject.look_direction == subject.RIGHT:
		damage_box.position.x += displacement
		if damage_box.position.x <= 0:
			return "pursue"
	elif subject.look_direction == subject.LEFT:
		damage_box.position.x -= displacement
		if damage_box.position.x >= 0:
			return "pursue"

func _damagebox_entered(object):
	if object:
		subject.attack()