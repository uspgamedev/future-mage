extends Creature

const DASH_SPEED := 500
var is_dashing := false
onready var player = self
onready var Projectile = preload("res://Projectile.tscn")
var direction:= Vector2(0, 0)
var bullet_offset := Vector2(0, 0)
var current_level
var is_dead:= false

func _ready():
	self.max_health = 5
	self.health = max_health
	self.move_speed = 250
	self.jump_speed = 600
	self.damage = 20
	self.enemies = ["enemy"]
	self.attack_offset = Vector2(80, 0)
	self.bullet_offset = Vector2(60, 0)
	
	for node in get_tree().get_root().get_children():
		if node.is_in_group("level"):
			current_level = node
			break

func _physics_process(delta):
	#print(health)
	if self.is_dead:
		return
	#print(is_dead)
	var direction = set_move_direction(delta)
	move(direction)
	if not stagger:
		if Input.is_action_pressed("ui_up"):
			jump()
		if Input.is_action_pressed("dash"):
			if Input.is_action_pressed("ui_down") and self._can_dash_down():
				self.set_collision_mask_bit(0, false)
				self.velocity.y = DASH_SPEED
			dash()
		if Input.is_action_just_pressed("Attack"):
			attack()
		if Input.is_action_just_pressed("Shoot"):
			shoot()

func set_move_direction(delta: float):
	if stagger:
		if knockback_direction.x > 0.0:
			direction.x = 1
		if knockback_direction.x < 0.0:
			direction.x = -1
		velocity.y = -100
		#print(direction)
		return direction
	
	direction.x = 0
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
	if Input.is_action_pressed("ui_right"):
		direction.x += 1

	if direction.x == 0:
		$AnimationPlayer.stop()
	direction.normalized()
	direction = (direction*delta).normalized()

	return direction

func move(direction: Vector2) -> void:
	if !is_dashing:
		.move(direction)
	else:
		move_and_slide(Vector2(direction.x * self.DASH_SPEED, self.velocity.y), Vector2(0,-1))

func dash():
	is_dashing = true
	$DashTimer.start()

func shoot():
	var bullet = Projectile.instance()
	self.add_child(bullet)
	if look_direction == RIGHT:
		bullet.global_position = self.global_position + bullet_offset
		bullet.get_node("Sprite").flip_h = false
	elif look_direction == LEFT:
		bullet.global_position = self.global_position - bullet_offset
		bullet.get_node("Sprite").flip_h = true

func take_damage(d):
	.take_damage(d)
	for i in range(0, d):
		$StaggerTimer.start()
	stagger = true

func die():
	if not is_dead:
		is_dead = true
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(2, false)
		$Sprite.rotate((3.1415926/2.0))

func _on_PassThrough_body_exited(body):
	set_collision_mask_bit(0, true)

func _on_DashTimer_timeout():
	is_dashing = false

func _can_dash_down():
	var floors = []
	for node in player.get_parent().get_children():
		if node.is_in_group("floor"):
			floors.append(node)

	for node in floors:
		if player in node.get_overlapping_bodies() or not player.is_on_floor():
			return false
	return true

func _on_StaggerTimer_timeout():
	stagger = false
