extends State

var move_direction: Vector2
var player: Node2D
var damage_box

func enter(obj):
	move_direction = Vector2(0, 0)
	player = obj.get_parent().get_node("Player")
	subject = obj
	damage_box = obj.get_node("./DamageBox")
	#print(self.name)
	
	sprite = obj.get_node("./Sprite")
	if obj.has_node("AnimationPlayer"):
			animation_player = obj.get_node("AnimationPlayer")

func update(delta):
	move_direction = player.position - subject.position
	
	if not _can_see_player():
		return "idle"
	if subject.movement_type == "jumping":
		return "jump"
	if abs(move_direction.x) <= 100:
		return "attack"

	if move_direction.x < 0:
		move_direction.x = -1
		sprite.set_flip_h(true)
	elif move_direction.x > 0:
		move_direction.x = 1
		sprite.set_flip_h(false)
	move_direction.y = 0
	
	if animation_player != null:
		animation_player.play("walk")
		
	move_direction.normalized()
	move_direction = (move_direction*delta).normalized()
	damage_box.position = move_direction.x * subject.attack_offset

	subject.move(move_direction)

func _can_see_player() -> bool:
	var direction = player.position - subject.position
	if abs(direction.x) <= 500 and direction.y <= 500 and direction.y >= -300:
		return true
	return false
