extends PlayerState

var gravity: int
var fall_speed: int

func enter(obj):
	.enter(obj)
	gravity = subject.get_parent().GRAVITY
	subject.direction.y = -1
	subject.velocity = Vector2(subject.move_speed * subject.direction.x, 
		subject.jump_speed * subject.direction.y)

func update(delta):
	_get_inputs()
	
	var should_fall: bool = false
	for ray in subject.ceilling_rays:
		should_fall = should_fall or ray.is_colliding()
	if should_fall or subject.velocity.y >= 0:
		return "fall"
	
	if input.left:
		subject.direction.x = -1
	if input.right:
		subject.direction.x = 1
	if input.dash and not subject.dash_timer > 0:
		return "dash"
	if input.shoot:
		subject.shoot()
	
	subject.direction = subject.direction.normalized()
	subject.velocity.x = subject.move_speed * subject.direction.x
	
	fall_speed += gravity/2
	subject.velocity.y += fall_speed
	
	subject.move_and_slide(subject.velocity, Vector2(0, -1))
	
	_clear_inputs()

