extends PlayerState

var velocity
var dash_speed:= 650
var timer = 1.0 / 3.0

func enter(obj):
	.enter(obj)
	
	subject.invulnerable = true
	if subject.look_direction == subject.RIGHT:
		subject.velocity = Vector2(dash_speed, 0)
	elif subject.look_direction == subject.LEFT:
		subject.velocity = Vector2(-dash_speed, 0)

func update(delta):
	_get_inputs()
	
	subject.move_and_slide(subject.velocity, Vector2(0, -1))
	timer -= delta
	
	if timer <= 0:
		return "fall"
	
	if not subject.floored:
		subject.velocity.y += 20
	
	_clear_inputs()

func exit():
	subject.invulnerable = false
	
	if not subject.dash_timer > 0:
		subject.dash_timer = 1.0
	
	.exit()