extends PlayerState

func enter(obj):
	.enter(obj)
	
	subject.direction = Vector2()
	subject.velocity = Vector2()
	
	subject.get_node("CollisionShape2D").disabled = true
	subject.rotate(3.1415926 / 2.0)
