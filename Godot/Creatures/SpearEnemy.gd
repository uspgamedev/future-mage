extends Creature

var states = {
	"idle": preload("res://States/NPCStates/Idle.tscn"),
	"pursue": preload("res://States/NPCStates/Pursue.tscn"),
	"fall": preload("res://States/NPCStates/Fall.tscn"),
	"stagger": preload("res://States/NPCStates/Stagger.tscn"),
	"attack": preload("res://States/NPCStates/Attack.tscn"),
}
var current_state: State
var next_state
var movement_type = "walking"
var player: Creature
var direction
var floored

signal hit

func _ready():
	self.max_health = 100
	self.health = max_health
	self.move_speed = 150
	self.enemies = ["player"]
	self.damage = 1
	
	connect("hit", self, "_push_stagger")
	
	for node in self.get_children():
		if node.is_in_group("floor_ray"):
			floor_rays.append(node)
	
	player = get_parent().get_node("Player")
	
	current_state = states.idle.instance()
	current_state.enter(self)

func _physics_process(delta):
	floored = false
	
	for ray in floor_rays:
		if ray.is_colliding():
			floored = true
	if player.current_state.name == "Dead" and next_state in ["pursue", "jump"]:
		next_state = "idle"
	if next_state != null:
		print(next_state)
		current_state.exit()
		current_state = states[next_state].instance()
		current_state.enter(self)
	next_state = current_state.update(delta)

func take_damage(damage):
	if current_state.name.to_lower() != "stagger":
		.take_damage(damage)
		emit_signal("hit")

func _push_stagger():
	next_state = "stagger"

func _on_DamageBox_body_entered(body):
	if body.is_in_group("player"):
		self.attack()
		body.knockback_direction = body.position - self.position