extends State

var move_direction: Vector2
var velocity: Vector2
var player
var gravity 

func enter(obj):
	subject = obj
	player = obj.get_parent().get_node("Player")
	move_direction = player.position - subject.position
	sprite = obj.get_node("Sprite")
	gravity = subject.get_parent().GRAVITY
	#print(self.name)
	
	subject.direction = player.position - subject.position
	
	subject.direction = subject.direction.normalized()
	subject.velocity.x = subject.move_speed * subject.direction.x
	
	if subject.direction.x > 0:
		subject.direction.x = 1
		sprite.set_flip_h(false)
	elif subject.direction.x < 0:
		subject.direction.x = -1
		sprite.set_flip_h(true)
	subject.direction.y = 0
	
	velocity = Vector2(subject.direction.x * subject.move_speed , -subject.jump_speed)

	if velocity.x > 0:
		subject.look_direction = subject.RIGHT
	elif velocity.x < 0:
		subject.look_direction = subject.LEFT

var fall_speed = 0
func update(delta):
	if not subject.floored and velocity.y > 0:
		return "fall"
	for ray in subject.ceilling_rays:
		if ray.is_colliding():
			return "fall"
	
	fall_speed += gravity
	velocity.y += fall_speed
	
	subject.move_and_slide(velocity, Vector2(0, -1))
	

func exit():
	subject.velocity = velocity