extends State

var knockback_direction:= Vector2(0, 1)
var timer: float = 0.0

func enter(obj):
	subject = obj
	self.knockback_direction = obj.knockback_direction

func update(delta):
	if knockback_direction.x <= 0.0:
		knockback_direction.x = -1
	if knockback_direction.x >= 0.0:
		knockback_direction.x = 1
	knockback_direction.x *= subject.move_speed
	knockback_direction.y = -100
	#knockback_direction = knockback_direction.normalized()
	
	subject.move_and_slide(knockback_direction, Vector2(0, -1))
	if timer >= 0.2:
		return "fall"
	timer += delta
	#print(timer)
