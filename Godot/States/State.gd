extends Node
class_name State

# obj parameter is the node to be controlled by the state
var subject: Creature
var sprite: Sprite
var animation_player: AnimationPlayer

func enter(obj):
	pass

func update(delta):
	return null

func exit():
	if animation_player != null:
		animation_player.stop()
	self.queue_free()