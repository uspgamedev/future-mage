extends PlayerState

func enter(obj):
	.enter(obj)
	subject.velocity.y = 0

func update(delta):
	_get_inputs()
	
	var should_fall: bool = false
	for ray in subject.floor_rays:
		should_fall = should_fall or ray.is_colliding()
	if not should_fall:
		return "fall"
	if input.jump:
		return "jump"
	if input.dash and not subject.dash_timer > 0:
		return "dash"
	if input.left:
		subject.direction.x = -1
	if input.right:
		subject.direction.x = 1
	if input.shoot:
		subject.shoot()
	subject.direction = subject.direction.normalized()
	subject.velocity = subject.move_speed * subject.direction

	subject.move_and_slide(subject.velocity)
	
	_clear_inputs()

