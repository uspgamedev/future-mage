extends State

var fall_speed: float
var velocity: Vector2
var move_direction: Vector2
var gravity

func enter(obj):
	subject = obj

	if subject.look_direction == subject.RIGHT:
		move_direction.x = 1
	elif subject.look_direction == subject.LEFT:
		move_direction.x = -1
	move_direction.y = 1
	velocity.y = 1

	gravity = subject.get_parent().GRAVITY

func update(delta):
	fall_speed += gravity if subject.velocity.y < 500 else 0
	subject.velocity.y += fall_speed if subject.velocity.y < 1000 else 0

	subject.move_and_slide(subject.velocity, Vector2(0, -1))

	if subject.floored:
		if subject.movement_type == "jumping":
			return "wait"
		elif subject.movement_type == "walking":
			return "idle"

func exit():
	subject.velocity = Vector2(0, 0)