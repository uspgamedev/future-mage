extends Creature

onready var Projectile = preload("res://Projectile.tscn")

var states = {
	"walk": preload("res://States/PlayerStates/Walk.tscn"),
	"jump": preload("res://States/PlayerStates/Jump.tscn"),
	"fall": preload("res://States/PlayerStates/Fall.tscn"),
	"staggered": preload("res://States/PlayerStates/Staggered.tscn"),
	"dash": preload("res://States/PlayerStates/Dash.tscn"),
	"dead": preload("res://States/PlayerStates/Dead.tscn"),
}
var current_state: State
var next_state
var movement_type = "walking"
var is_dead = false
var direction: Vector2
var invulnerable: bool
var floored: bool
var shoot_timer: float
var dash_timer:= 0.0

func _ready():
	current_state = states.walk.instance()
	current_state.enter(self)
	self.jump_speed = 1000
	self.max_health = 5
	self.health = max_health
	self.move_speed = 250
	self.attack_offset = Vector2(0, 0)
	self.direction = Vector2(0, 0)
	self.enemies = ['enemy']
	for node in self.get_children():
		if node.is_in_group('floor_ray'):
			floor_rays.append(node)
	for node in self.get_children():
		if node.is_in_group('ceilling_ray'):
			ceilling_rays.append(node)

func _physics_process(delta):
	if shoot_timer > 0:
		shoot_timer -= delta
	if dash_timer > 0:
		dash_timer -= delta
	
	floored = false
	for ray in floor_rays:
		if ray.is_colliding():
			floored = true
	
	direction = Vector2()
	set_look_direction()

	dash_timer -= delta
	#print(next_state)
	if next_state != null:
		current_state.exit()
		current_state = states[next_state].instance()
		current_state.enter(self)
	next_state = current_state.update(delta)

func shoot():
	if shoot_timer > 0:
		return
	var bullet = Projectile.instance()
	self.add_child(bullet)
	if look_direction == RIGHT:
		bullet.global_position = self.global_position + Vector2(60, 0)
		bullet.get_node("Sprite").flip_h = false
	elif look_direction == LEFT:
		bullet.global_position = self.global_position - Vector2(60, 0)
		bullet.get_node("Sprite").flip_h = true
	shoot_timer = 0.25

func set_look_direction():
	if velocity.x > 0:	
		look_direction = RIGHT
		$Sprite.set_flip_h(false)
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.play("walk")
	elif velocity.x < 0:
		look_direction = LEFT
		$Sprite.set_flip_h(true)
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.play("walk")
	else:
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.stop()

func take_damage(damage):
	if not invulnerable:
		#print("player taking damage")
		.take_damage(damage)
		next_state = "staggered"

func die():
	next_state = "dead"