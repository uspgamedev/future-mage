extends State
class_name PlayerState

func enter(obj):
	subject = obj

var input = {
	"left": null,
	"right": null,
	"jump": null,
	"dash": null,
	"shoot": null,
}

func _get_inputs():
	if Input.is_action_pressed("ui_left"):
		input.left = true
	if Input.is_action_pressed("ui_right"):
		input.right = true
	if Input.is_action_pressed("ui_up"):
		input.jump = true
	if Input.is_action_just_pressed("dash"):
		input.dash = true
	if Input.is_action_just_pressed("Shoot"):
		input.shoot = true

func _clear_inputs():
	input.left = false
	input.right = false
	input.jump = false
	input.dash = false
	input.shoot = false
