extends State

var aggro_range: int
var player: Node2D

func enter(obj):
	player = obj.get_parent().get_node("Player")
	subject = obj
	#print(self.name)

func update(delta):
	var direction = player.position - subject.position
	if not subject.floored:
		return "fall"
	elif abs(direction.x) <= 500 and direction.y <= 500 and direction.y >= -500:
		return "pursue"
