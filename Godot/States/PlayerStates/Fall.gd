extends PlayerState

var gravity: float
var fall_speed: float

func enter(obj):
	.enter(obj)
	gravity = subject.get_parent().GRAVITY
	subject.direction.y = 1
	subject.velocity.y = 0

func update(delta):
	_get_inputs()
	
	if input.left:
		subject.direction.x = -1
	if input.right:
		subject.direction.x = 1
	if input.shoot:
		subject.shoot()
	if input.dash and not subject.dash_timer > 0:
		return "dash"
	
	subject.direction = subject.direction.normalized()
	subject.velocity.x = subject.move_speed * subject.direction.x
	
	fall_speed += gravity if subject.velocity.y < 500 else 0
	subject.velocity.y += fall_speed if subject.velocity.y < 1000 else 0
	
	subject.move_and_slide(subject.velocity, Vector2(0, -1))

	var should_walk: bool = false
	for ray in subject.floor_rays:
		should_walk = should_walk or ray.is_colliding()
	if should_walk:
		#print(subject.get_node("./FloorRay").get_collider())
		return "walk"
	
	_clear_inputs()

func exit():
	subject.direction = Vector2()
	subject.velocity = Vector2()
