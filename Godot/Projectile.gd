extends KinematicBody2D

var bullet_speed:= 1000
var damage:= 20
var direction: Vector2
onready var player = get_parent()

func _ready():
	set_as_toplevel(true)
	if player.look_direction == player.RIGHT:
		direction = Vector2(1, 0)
	if player.look_direction == player.LEFT:
		direction = Vector2(-1, 0)
	#print("shoot")

# warning-ignore:unused_argument
func _physics_process(delta):
	var velocity = direction * bullet_speed
# warning-ignore:return_value_discarded
	move_and_slide(velocity)

# warning-ignore:unused_argument
func _on_hit(body):
	var bodies = $DamageBox.get_overlapping_bodies()
	for body in bodies:
		for enemy in player.enemies:
			if body.is_in_group(enemy):
				body.take_damage(damage)
				body.knockback_direction = body.position - self.position
				if body.health <= 0:
					body.die()
	self.queue_free()
	
