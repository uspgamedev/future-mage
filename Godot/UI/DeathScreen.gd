extends Control

var first_level = load("res://Levels/Level1.tscn")
var main_screen = load("res://UI/TitleScreen.tscn")

#not working
func _on_RestartButton_pressed():
	var first_level = first_level.instance()
	for node in get_tree().get_root().get_children():
		node.queue_free()
	get_tree().get_root().add_child(first_level)
	self.queue_free()
	get_tree().paused = false

func _on_MainScreenButton_pressed():
	var menu = main_screen.instance()
	for node in get_tree().get_root().get_children():
		if node != self:
			node.queue_free()
	get_tree().get_root().add_child(menu)
	self.queue_free()
	get_tree().paused = false
