extends PlayerState

var initial_stagger: bool
var knockback_speed: int
var direction: Vector2
var timer: float

# state that changes to stagger sets direction
func enter(obj):
	.enter(obj)
	
	if subject.knockback_direction.x >= 0:
		direction.x = 1
	else:
		direction.x = -1
	direction.y = -1
	direction = direction.normalized()
	
	knockback_speed = subject.move_speed * 2

	initial_stagger = true
	timer = 0.25

func update(delta):
	if initial_stagger:
		if timer <= 0:
			initial_stagger = false
		subject.move_and_slide(direction * knockback_speed)
		timer -= delta
	else:
		return "fall"
