extends Node2D

#const GRAVITY := 1000 #for actual game
const GRAVITY := 10 #for LevelTeste

var objects = []
var PauseMenu = preload("res://UI/PauseMenu.tscn")
var UI = preload("res://UI/UI.tscn")
var ui_layer
var Lifebar = preload("res://UI/LifeBar.tscn")
var Life = preload("res://UI/OneLife.tscn")
var DeathScreen = preload("res://UI/DeathScreen.tscn")
var ui
var player: Creature

func _ready():
	ui_layer = CanvasLayer.new()
	ui_layer.layer = 1
	get_tree().get_root().add_child(ui_layer)
	
	ui = UI.instance()
	
	player = get_node("Player")
	
	for i in range(0, player.health):
		ui.get_node("LifeBar").add_child(Life.instance())

	ui_layer.add_child(ui)

func _physics_process(delta):
	if Input.is_action_pressed("Pause"):
		var pause_menu = PauseMenu.instance()
		ui_layer.add_child(pause_menu)
		get_tree().paused = true
	
	if player.current_state.name == "Dead":
		var deathscreen = DeathScreen.instance()
		ui_layer.add_child(deathscreen)
		get_tree().paused = true
	
	_update_life_bar()

func _update_life_bar():
	var lives = ui.get_node("LifeBar").get_children()
	if not lives.empty():
		var diff = len(lives) - player.health
		for i in range(0, diff):
			lives[len(lives)-1].queue_free()

func _on_Doom_body_entered(body):
	body.die()
