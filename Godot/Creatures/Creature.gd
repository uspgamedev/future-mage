extends KinematicBody2D
class_name Creature

var max_health: int
var health: int
var move_speed: int
var jump_speed: int
var damage: int
var velocity: Vector2
var look_direction: int
enum { RIGHT, LEFT }
# warning-ignore:unused_class_variable
var attack_offset: Vector2
var enemies = []
var stagger:= false
var knockback_direction
var floor_rays = []
var ceilling_rays = []

# warning-ignore:unused_argument
func set_move_direction(delta: float) -> Vector2:
	return Vector2()

func move(direction: Vector2) -> void:
	if stagger:
		direction.x *= 2
	self.velocity = Vector2(direction.x * self.move_speed, self.velocity.y)	
	if velocity.x > 0 and not stagger:	
		look_direction = RIGHT
		$Sprite.set_flip_h(false)
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.play("walk")
	elif velocity.x < 0 and not stagger:
		look_direction = LEFT
		$Sprite.set_flip_h(true)
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.play("walk")
	else:
		if self.has_node("AnimationPlayer"):
			$AnimationPlayer.stop()
		
	velocity = move_and_slide(self.velocity, Vector2(0,-1))

func jump() -> void:
	if self.velocity.y == 0:
		velocity.y = -jump_speed

func attack() -> void:
	var bodies = $DamageBox.get_overlapping_bodies()
	for body in bodies:
		for enemy in enemies:
			if body.is_in_group(enemy):
				body.take_damage(damage)
				if body.health <= 0:
					body.die()

func take_damage(damage: int) -> void:
	self.health -= damage
# warning-ignore:narrowing_conversion
	health = clamp(health, 0, max_health)

func heal(heal_amount: int) -> void:
	self.health += heal_amount
# warning-ignore:narrowing_conversion
	self.health = clamp(health, 0, max_health)

func die() -> void:
	self.queue_free()
