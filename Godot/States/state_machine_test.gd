extends Creature

var states = {
	"idle": preload("res://States/NPCStates/Idle.tscn"),
	"pursue": preload("res://States/NPCStates/Pursue.tscn"),
	"jump": preload("res://States/NPCStates/Jump.tscn"),
	"fall": preload("res://States/NPCStates/Fall.tscn"),
	"wait": preload("res://States/NPCStates/Wait.tscn"),
	"attack": preload("res://States/NPCStates/Attack.tscn")
}
var current_state: State
var next_state
var movement_type = "walking"

func _ready():
	current_state = states.idle.instance()
	current_state.enter(self)
	self.jump_speed = 500
	self.max_health = 200
	self.health = max_health
	self.move_speed = 75
	self.enemies = ["player"]
	self.attack_offset = Vector2(0, 0)

func _physics_process(delta):
	next_state = current_state.update(delta)
	if next_state != null:
		current_state.exit()
		current_state = states[next_state].instance()
		current_state.enter(self)
